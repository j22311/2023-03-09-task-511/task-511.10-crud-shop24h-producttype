///khai báo thưu viện Express
const express = require('express');
//khai báo app để chạy
const app = express();
// khai báo port để chạy trên post man 
const port = 8000;
// Cấu hình request đọc được body json
app.use(express.json());
//hàm này chạy ghi ra ngày tháng trong terminal
app.use((req,res,next) => {
    console.log(new Date());

    next();
}) 
//-----b1khai báo thư viện mongoose-----
var mongoose = require('mongoose');
const { customerRouter } = require('./app/routes/customerRouter');
const { orderDetailRouter } = require('./app/routes/orderDetailRouter');
const { orderRouter } = require('./app/routes/orderRouter');
//import lớp ROuter vào
const { productRouter } = require('./app/routes/productRouter');

const { productTypeRouter } = require('./app/routes/productTypeRouter');
// //khai báo drinksRounter để chạy trên postMan Task 511.10 CRUD Shop24h ProductType
app.use('/',productTypeRouter)

// //khai báo voucherRounter để chạy trên postMan Task 511.20 CRUD Shop24h Product

app.use('/',productRouter);

// //Task 511.30 CRUD Shop24h Customer

 app.use('/',customerRouter);

 app.use('/',orderRouter)

 app.use('/',orderDetailRouter)

// //khai báo orderRouter để chạy trên postMan Task 506.60 Pizza365 Drink router 
// app.use('/',orderRouter)
// -----khỡi động app qua cổng------
app.listen(port, () => {
    console.log(`app listen on port ${port}`);
}) 



// b2: kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_Shop24h');
  console.log('Successfully connected mongoDB');
  
}