//khai báo thư viện 
const express = require('express');
const { getAllProduct, getAProductById, insertAProduct, updateAProductById, deleteAProductById } = require('../controllers/productController');

//khai báo một biến ROuter chạy trên file app.js
const productRouter = express.Router();

productRouter.get('/product/',getAllProduct)

productRouter.get('/product/:productId', getAProductById)


productRouter.post('/product/',insertAProduct)



productRouter.put('/producttype/:productId',updateAProductById)

productRouter.delete('/product/:productId',deleteAProductById)

module.exports = {
    productRouter
}