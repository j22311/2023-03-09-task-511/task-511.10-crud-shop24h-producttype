//khai báo thư viện 
const express = require('express');
const { getAllOrder, getAnOrderById, insertAnOrder, updateAnOrderById, deleteAnOrderById } = require('../controllers/orderController');

//khai báo một biến ROuter chạy trên file app.js
const orderRouter = express.Router();

orderRouter.get('/order/',getAllOrder)

orderRouter.get('/order/:orderId', getAnOrderById)


orderRouter.post('/order/',insertAnOrder)



orderRouter.put('/ordertype/:orderId',updateAnOrderById)

orderRouter.delete('/order/:orderId',deleteAnOrderById)

module.exports = {
    orderRouter
}