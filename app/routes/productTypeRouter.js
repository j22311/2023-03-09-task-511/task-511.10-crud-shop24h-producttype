//khai báo thư viện 
const express = require('express');
const { insertAProductType, getAllProductType, getAProductTypeById, updateAProductTypeById, deleteAProductTypeById } = require('../controllers/productTypeController');

//khai báo một biến ROuter chạy trên file app.js
const productTypeRouter = express.Router();

productTypeRouter.get('/producttype/',getAllProductType)

productTypeRouter.get('/producttype/:productTypeId', getAProductTypeById)

// productTypeRouter.post('/producttype/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })
productTypeRouter.post('/producttype/',insertAProductType)



productTypeRouter.put('/producttype/:productTypeId',updateAProductTypeById)

productTypeRouter.delete('/producttype/:productTypeId',deleteAProductTypeById)

module.exports = {
    productTypeRouter
}