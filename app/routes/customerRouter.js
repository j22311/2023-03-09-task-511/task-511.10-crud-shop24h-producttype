//khai báo thư viện 
const express = require('express');
const { getAllCustomer, getAnCustomerById, createCustomer, updateCustomerById, deleteCustomerByID } = require('../controllers/customerController');

//khai báo một biến ROuter chạy trên file index.js
const customerRouter = express.Router();

customerRouter.get('/customers/',getAllCustomer);

customerRouter.get('/customers/:customerId',getAnCustomerById);

customerRouter.post('/customers/',createCustomer);
// customerRouter.post('/customers/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

customerRouter.put('/customers/:customerId',updateCustomerById);

customerRouter.delete('/customers/:customerId',deleteCustomerByID);

module.exports = {
    customerRouter
}