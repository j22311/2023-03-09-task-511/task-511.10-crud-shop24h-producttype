//khai báo thư viện 
const express = require('express');
const { getAllorderDetail, getAnorderDetailById, insertAnorderDetail, updateAnorderDetailById, deleteAnorderDetailById } = require('../controllers/orderDetailController');

//khai báo một biến ROuter chạy trên file app.js
const orderDetailRouter = express.Router();

orderDetailRouter.get('/orderDetail/',getAllorderDetail)

orderDetailRouter.get('/orderDetail/:orderDetailId', getAnorderDetailById)


orderDetailRouter.post('/orderDetail/',insertAnorderDetail)



orderDetailRouter.put('/orderDetailtype/:orderDetailId',updateAnorderDetailById)

orderDetailRouter.delete('/orderDetail/:orderDetailId',deleteAnorderDetailById)

module.exports = {
    orderDetailRouter
}