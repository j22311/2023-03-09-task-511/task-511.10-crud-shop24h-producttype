//import thư viện mongoose
// const { response, request } = require('express');
const mongoose = require('mongoose');

//import model productType
const productTypeModel = require('../models/productTypeModel');

const getAllProductType = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    productTypeModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all productType',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAProductTypeById = (request,response) => {
    //b1: thu thập dữ liệu
    const productTypeId = request.params.productTypeId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${productTypeId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    productTypeModel.findById(productTypeId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
 //---------------------------insert a drink
 const insertAProductType = (request, response) => {
   
    // b1: thu thập dữ liệu
    const body = request.body;
     // b2: validate dữ liệu 
    if(!body.name){
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    
     // b3: gọi model tạo dữ liệu 
     const newProductType = new productTypeModel( {
       //  _id: mongoose.Types.ObjectId,
       name : body.name,
       description: body.description
      
     })
     console.log(newProductType);
    
     productTypeModel.create(newProductType)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new ProductType',
             productType: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
  }

  //-----------------update a ProductTyp----------------------------
const updateAProductTypeById = (request,response) => {
    //b1: thu thập dữ liệu
    const ProductTypeId = request.params.productTypeId;
    const body = request.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductTypeId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${drinkId} ko hợp lệ`
       })
    }
    if(!body.name){
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
     // b3: gọi model để update dữ liệu
     let newProductTypeUpdate = {
       name : body.name,
       description : body.description,
       
     }
     productTypeModel.findByIdAndUpdate(ProductTypeId,newProductTypeUpdate)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new ProductType',
             drink: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
 
 }

 //--------------delete a productType by ID----------------------
const deleteAProductTypeById = ((request,response) => {
    //b1: thu thập dữ liệu
    const productTypeId = request.params.productTypeId;
    //b2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${productTypeId} ko hợp lệ`
       })
    }
    //b3: gọi model xóa dữ liệu
    productTypeModel.findByIdAndDelete(productTypeId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete a productType ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
 })

 module.exports = {
    deleteAProductTypeById,
    updateAProductTypeById,
    insertAProductType,
    getAProductTypeById,
    getAllProductType
 }


