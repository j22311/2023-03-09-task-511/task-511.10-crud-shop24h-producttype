//import thư viện mongoose
// const { response, request } = require('express');
const mongoose = require('mongoose');

//import model orderType
const orderModel = require('../models/orderModel');

const getAllOrder = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    orderModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all order',
         order: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnOrderById = (request,response) => {
    //b1: thu thập dữ liệu
    const orderId = request.params.orderId;
    const orderTypeId = request.params.orderTypeId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${orderId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    orderModel.findById(orderId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             order: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
 //---------------------------insert a drink
 const insertAnOrder = (request, response) => {
   
    // b1: thu thập dữ liệu
    const body = request.body;
 
    //b2: Validate dữ liệu
   
     
    if(!body.orderDate){
        console.log(typeof(body.orderDate));
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    
     if(isNaN(body.cost) || body.cost <= 0 ){
        return response.status(400).json({
           status: 'Bad request',
           message: 'cost ko hop le'
        })
     }


     // b3: gọi model tạo dữ liệu 
     const newOrder = new orderModel( {
       
        orderDate : body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetail: body.orderDetail,
        cost: body.cost,
        country: body.country,
      
     })
     console.log(newOrder);
    
     orderModel.create(newOrder)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new order',
             order: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
  }

  //-----------------update a orderTyp----------------------------
const updateAnOrderById = (request,response) => {
    //b1: thu thập dữ liệu
    const orderId = request.params.orderId;

    const body = request.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${orderId} ko hợp lệ`
       })
    }
      
    if(!body.orderDate){
        console.log(typeof(body.orderDate));
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    
     if(isNaN(body.cost) || body.cost <= 0 ){
        return response.status(400).json({
           status: 'Bad request',
           message: 'cost ko hop le'
        })
     }
 
 
     // b3: gọi model để update dữ liệu
     let newOrderUpdate = {
        orderDate : body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        orderDetail: body.orderDetail,
        cost: body.cost,
        country: body.country,
     }
     orderModel.findByIdAndUpdate(orderId,newOrderUpdate)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new order',
             order: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
 
 }

 //--------------delete a order by ID----------------------
const deleteAnOrderById = ((request,response) => {
    //b1: thu thập dữ liệu
    const orderId = request.params.orderId;
    //b2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${orderId} ko hợp lệ`
       })
    }
    //b3: gọi model xóa dữ liệu
    orderModel.findByIdAndDelete(orderId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete a order ',
          order: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
 })

 module.exports = {
    deleteAnOrderById,
    updateAnOrderById,
    insertAnOrder,
    getAnOrderById,
    getAllOrder
 }


