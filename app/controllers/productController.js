//import thư viện mongoose
// const { response, request } = require('express');
const mongoose = require('mongoose');

//import model productType
const productModel = require('../models/productModel');

const getAllProduct = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    productModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all product',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAProductById = (request,response) => {
    //b1: thu thập dữ liệu
    const productId = request.params.productId;
    const productTypeId = request.params.productTypeId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${productId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    productModel.findById(productId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             product: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
 //---------------------------insert a drink
 const insertAProduct = (request, response) => {
   
    // b1: thu thập dữ liệu
    const body = request.body;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.type)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${productTypeId} ko hợp lệ`
       })
    }
     
    if(!body.name){
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    if(!body.description){
        return response.status(400).json({
           status: 'Bad request',
           message: 'description ko hop le'
        })
     }
     if(!body.type){
        return response.status(400).json({
           status: 'Bad request',
           message: 'description ko hop le'
        })
     }
     if(!body.imageUrl){
        return response.status(400).json({
           status: 'Bad request',
           message: 'description ko hop le'
        })
     }
     if(NaN(body.buyPrice) || body.buyPrice < 0){
        return response.status(400).json({
           status: 'Bad request',
           message: 'buyPrice ko hop le'
        })
     }
     if(NaN(body.promotionPrice) || body.promotionPrice < 0){
        return response.status(400).json({
           status: 'Bad request',
           message: 'buyPrice ko hop le'
        })
     }
     if(NaN(body.amount) ){
        return response.status(400).json({
           status: 'Bad request',
           message: 'amount ko hop le'
        })
     }


     // b3: gọi model tạo dữ liệu 
     const newProduct = new productModel( {
       
       name : body.name,
       description: body.description,
       type: body.type,
       imageUrl: body.imageUrl,
       buyPrice: body.buyPrice,
       promotionPrice: body.promotionPrice,
       amount: body.amount,
        type: body.type
     })
     console.log(newProduct);
    
     productTypeModel.create(newProduct)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new Product',
             product: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
  }

  //-----------------update a ProductTyp----------------------------
const updateAProductById = (request,response) => {
    //b1: thu thập dữ liệu
    const ProductId = request.params.productId;

    const body = request.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductTypeId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${drinkId} ko hợp lệ`
       })
    }
    if(!mongoose.Types.ObjectId.isValid(body.type)){
        return response.status(400).json({
           status: 'Bad Request',
           message: `${productTypeId} ko hợp lệ`
        })
     }
      
     if(!body.name){
        return response.status(400).json({
           status: 'Bad request',
           message: 'name ko hop le'
        })
     }
     if(!body.description){
         return response.status(400).json({
            status: 'Bad request',
            message: 'description ko hop le'
         })
      }
      if(!body.type){
         return response.status(400).json({
            status: 'Bad request',
            message: 'description ko hop le'
         })
      }
      if(!body.imageUrl){
         return response.status(400).json({
            status: 'Bad request',
            message: 'description ko hop le'
         })
      }
      if(NaN(body.buyPrice) || body.buyPrice < 0){
         return response.status(400).json({
            status: 'Bad request',
            message: 'buyPrice ko hop le'
         })
      }
      if(NaN(body.promotionPrice) || body.promotionPrice < 0){
         return response.status(400).json({
            status: 'Bad request',
            message: 'buyPrice ko hop le'
         })
      }
      if(NaN(body.amount) ){
         return response.status(400).json({
            status: 'Bad request',
            message: 'amount ko hop le'
         })
      }
 
 
     // b3: gọi model để update dữ liệu
     let newProductUpdate = {
        name : body.name,
       description: body.description,
       type: body.type,
       imageUrl: body.imageUrl,
       buyPrice: body.buyPrice,
       promotionPrice: body.promotionPrice,
       amount: body.amount,
        type: body.type
       
     }
     productTypeModel.findByIdAndUpdate(ProductId,newProductUpdate)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new Product',
             product: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
 
 }

 //--------------delete a product by ID----------------------
const deleteAProductById = ((request,response) => {
    //b1: thu thập dữ liệu
    const productId = request.params.productId;
    //b2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${productId} ko hợp lệ`
       })
    }
    //b3: gọi model xóa dữ liệu
    productTypeModel.findByIdAndDelete(productId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete a product ',
          product: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
 })

 module.exports = {
    deleteAProductById,
    updateAProductById,
    insertAProduct,
    getAProductById,
    getAllProduct
 }


