// Import thư viện mongoose
const mongoose = require("mongoose");

// Import customers Model
const customerModel = require('../models/customerModel');

const getAllCustomer = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    customerModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all customers',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnCustomerById = (request,response) => {
    //b1: thu thập dữ liệu
    const customerId = request.params.customerId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${customerId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    customerModel.findById(customerId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!body.fullName) {
        console.log(body.fullName);
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
            
        })
    }

    // Kiểm tra noStudent có hợp lệ hay không

    if(!body.phone) {
        console.log(body.phone);
        return response.status(400).json({
            status: "Bad Request",
            message: `phone không hợp lệ ${body.phone}`
        })
    }

    if(!body.email ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }

    if(!body.address ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }


    // B3: Gọi Model tạo dữ liệu
    const newCustomer = {
       
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    }

    customerModel.create(newCustomer)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newcustomer',
             productType: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateCustomerById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }

    if(body.fullName !== undefined && body.fullName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    if(body.email !== undefined && body.email.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }
    if(body.address !== undefined && body.address.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }
    if(body.city !== undefined  && body.city.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "city không hợp lệ"
        })
    }
    if(body.country !== undefined  && body.country.trim() === "" ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "country không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const updateCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country
    }

    

    customerModel.findByIdAndUpdate(customerId,updateCustomer)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new customer',
             customer: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteCustomerByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    customerModel.findByIdAndDelete(customerId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an customer ',
          customer: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteCustomerByID,
    updateCustomerById,
    createCustomer,
    getAnCustomerById,
    getAllCustomer
}