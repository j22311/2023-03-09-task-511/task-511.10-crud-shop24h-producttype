//import thư viện mongoose
// const { response, request } = require('express');
const mongoose = require('mongoose');

//import model orderDetailType
const orderDetailModel = require('../models/orderDetailModel');

const getAllorderDetail = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    orderDetailModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all orderDetail',
         orderDetail: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnorderDetailById = (request,response) => {
    //b1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;
    const orderDetailTypeId = request.params.orderDetailTypeId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${orderDetailId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    orderDetailModel.findById(orderDetailId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             orderDetail: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
 //---------------------------insert a drink
 const insertAnorderDetail = (request, response) => {
   
    // b1: thu thập dữ liệu
    const body = request.body;
 
    //b2: Validate dữ liệu
   
     
    if(!mongoose.Types.ObjectId.isValid(body.product)){
        console.log(typeof(body.orderDetailDate));
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    
     if(isNaN(body.quantity) || body.quantity <= 0 ){
        return response.status(400).json({
           status: 'Bad request',
           message: 'cost ko hop le'
        })
     }


     // b3: gọi model tạo dữ liệu 
     const neworderDetail = new orderDetailModel( {
       
        product : body.product,
        quantity: body.quantity,
        
      
     })
     console.log(neworderDetail);
    
     orderDetailModel.create(neworderDetail)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new orderDetail',
             orderDetail: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
  }

  //-----------------update a orderDetailTyp----------------------------
const updateAnorderDetailById = (request,response) => {
    //b1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;

    const body = request.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${orderDetailId} ko hợp lệ`
       })
    }
      
    if(!mongoose.Types.ObjectId.isValid(body.product)){
        console.log(typeof(body.orderDetailDate));
       return response.status(400).json({
          status: 'Bad request',
          message: 'name ko hop le'
       })
    }
    
     if(isNaN(body.quantity) || body.quantity <= 0 ){
        return response.status(400).json({
           status: 'Bad request',
           message: 'cost ko hop le'
        })
     }

 
     // b3: gọi model để update dữ liệu
     let neworderDetailUpdate = {
        product : body.product,
        quantity: body.quantity,
        
     }
     orderDetailModel.findByIdAndUpdate(orderDetailId,neworderDetailUpdate)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new orderDetail',
             orderDetail: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
 
 }

 //--------------delete a orderDetail by ID----------------------
const deleteAnorderDetailById = ((request,response) => {
    //b1: thu thập dữ liệu
    const orderDetailId = request.params.orderDetailId;
    //b2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${orderDetailId} ko hợp lệ`
       })
    }
    //b3: gọi model xóa dữ liệu
    orderDetailModel.findByIdAndDelete(orderDetailId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete a orderDetail ',
          orderDetail: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
 })

 module.exports = {
    deleteAnorderDetailById,
    updateAnorderDetailById,
    insertAnorderDetail,
    getAnorderDetailById,
    getAllorderDetail
 }


