//b1 khai báo thư viện mongoosedb
const mongoose = require('mongoose');

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const orderSChema = new Schema({
    //_id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
        
    },
    shippedDate: {
    type: Date,
    
        
    },
    note: {
        type: String,
        
    },
    orderDetail: {
        type: mongoose.Types.ObjectId,
        ref: "OrderDetails"
    },
    cost:{
        type: Number,
        default: 0
    },
    country:{
        type: String,
        default: 0
    },
   
    
})

module.exports = mongoose.model("Orders",orderSChema);