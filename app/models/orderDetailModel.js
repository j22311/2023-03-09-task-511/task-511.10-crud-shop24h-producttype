//b1 khai báo thư viện mongoosedb
const mongoose = require('mongoose');

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const orderDetailSChema = new Schema({
    //_id: mongoose.Types.ObjectId,
   
    product: {
        type: mongoose.Types.ObjectId,
        ref: 'orderDetailtypes',
        require: true
    },
   
    quantity:{
        type: Number,
        default: 0
    }
    
})

module.exports = mongoose.model("OrderDetails",orderDetailSChema);