//b1 khai báo thư viện mongoosedb
const mongoose = require('mongoose');

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const productTypeSChema = new Schema({
    //_id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String,
        
    },
    
})

module.exports = mongoose.model("productTypes",productTypeSChema);