//b1 khai báo thư viện mongoosedb
const mongoose = require('mongoose');

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const customerSChema = new Schema({
    //_id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        
        require: true
    },
   phone: {
    type: String,
    unique: true,
    require: true
        
    },
    email: {
        type: String,
        unique: true,
        require: true
    },
    address: {
        type: String,
        default: ""
    },
    city:{
        type: String,
        default: ""
    },
    country:{
        type: String,
        default: ""
    },
   orders:[{
        type: mongoose.Types.ObjectId,
        ref: 'Orders'
    }]
    
})

module.exports = mongoose.model("Customers",customerSChema);