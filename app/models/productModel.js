//b1 khai báo thư viện mongoosedb
const mongoose = require('mongoose');

//b2: khai báo một Schema để tạo một Schema
const Schema = mongoose.Schema;

//b3 tạo đối tượng schema gồm các thuộc tính colecction trong mongbd
const productSChema = new Schema({
    //_id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String,
        
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: 'producttypes',
        require: true
    },
    imageUrl: {
        type: String,
        require: true
    },
    buyPrice:{
        type: Number,
        require: true
    },
    promotionPrice:{
        type: Number,
        require: true
    },
    amount:{
        type: Number,
        default: 0
    }
    
})

module.exports = mongoose.model("products",productSChema);